<?php

namespace App\Models\Post;

use App\Structs\Post\PostStruct;
use Illuminate\Database\Eloquent\Concerns\HasUlids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Query\Builder;
use Illuminate\Pagination\LengthAwarePaginator;

class PostModel extends Model
{
    use HasFactory, HasUlids;

    protected $connection = "pgsql";
    protected $table      = "posts";

    public static function checkPostExist(string $id): bool
    {
        return self::query()
            ->where('id', $id)
            ->exists();
    }

    public static function doAdd(array $data): bool
    {
        return self::query()->insert($data);
    }

    public static function doGet(array $filter): LengthAwarePaginator
    {
        $query = self::query()
            ->orderBy($filter['sort_by'] ?? 'created_at', $filter['sort'] ?? 'desc')
            ->where(function ($query) use ($filter) {
                if ($filter['search_by']) {
                    foreach (array_keys($filter['search_by']) as $key) {
                        if($key == 'type'&& $filter['search_by'][$key] == 'Tất cả'){
                            continue;
                        }
                        if ($key == 'monthly_rent_cost' || $key == 'area') {
                            $price = preg_split('/[^0-9.]+/', $filter['search_by'][$key]);
                            if(count($price) == 1){
                                $query->where($key, '>=' ,$filter['search_by'][$key]);
                            }
                            else {
                                $query->where($key, '>=',$price[0]);
                                $query->where($key, '<',$price[1]);
                            }
                        } else if ($key == 'additional_information') {
                            foreach (array_keys($filter['search_by'][$key]) as $additional_key) {
                                $query->where($key . '->' . $additional_key, $filter['search_by'][$key][$additional_key]);
                            }
                        } else if ($key == 'province' || $key == 'district') {
                            $query->where('address', 'LIKE', '%' . $filter['search_by'][$key] . '%');
                        } else {
                            $query->where($key, 'LIKE', '%' . $filter['search_by'][$key] . '%');
                        }
                    }
                }
//                $query->where('user_id', $filter['user_id']);
            });
        return $query->paginate($filter['limit'], $filter['fields'], "{$filter['page']}", $filter['page']);
    }

    public static function doGetById(string $id, array $filter, ?string $ref = null): Model|Builder|null
    {
        return self::query()
            ->where(function ($query) use ($id, $ref) {
                $query->where('id', $id);
                if ($ref) {
                    $query->where('user_id', $ref);
                }
            })
            ->distinct()
            ->first($filter);
    }

    public static function doEdit(array $data, PostModel $post): bool
    {
        $post->forceFill($data);

        return $post->save();

    }
    public static function doDelete(PostModel $post):bool
    {
        return $post->delete();
    }
//    public static function doEdit(array $data, PostModel $diary): bool
//    {
//        $diary->forceFill($data);
//
//        return $diary->save();
//
//    }

//    public static function doDelete(PostModel $diary):bool
//    {
//        $diary->forceFill(['status' => 0]);
//
//        return $diary->save();
//    }
    public function struct(): PostStruct
    {
        return new PostStruct($this->getAttributes());
    }


}
