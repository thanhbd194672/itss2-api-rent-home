<?php

namespace App\Structs\Post;

use App\Libs\Serializer\Normalize;
use App\Models\Image\UseImage;
use App\Structs\Struct;
use Illuminate\Support\Carbon;

class PostStruct extends Struct
{
	public ?float $area;
	public ?object $image;
	public ?object $cover_image;
	public ?object $additional_information;
	public ?Carbon $created_at;
	public ?Carbon $updated_at;
	public ?int $monthly_rent_cost;
	public ?string $owner_phone_number;
	public ?string $id;
	public ?string $address;
	public ?string $user_id;
	public ?string $title;
	public ?string $type;
	public ?string $rental_status;
	public ?string $description;
	public ?string $owner_name;
	public function __construct(object|array $data)
	{
		if (is_object($data)) {
			$data = $data->toArray();
		}

		$this->area = Normalize::initFloat($data, 'area');
		$this->image = Normalize::initObject($data, 'image');
		$this->cover_image = Normalize::initObject($data, 'cover_image');
		$this->additional_information = Normalize::initObject($data, 'additional_information');
		$this->created_at = Normalize::initCarbon($data, 'created_at');
		$this->updated_at = Normalize::initCarbon($data, 'updated_at');
		$this->monthly_rent_cost = Normalize::initInt($data, 'monthly_rent_cost');
		$this->owner_phone_number = Normalize::initString($data, 'owner_phone_number');
		$this->id = Normalize::initString($data, 'id');
		$this->address = Normalize::initString($data, 'address');
		$this->user_id = Normalize::initString($data, 'user_id');
		$this->title = Normalize::initString($data, 'title');
		$this->type = Normalize::initString($data, 'type');
		$this->rental_status = Normalize::initString($data, 'rental_status');
		$this->description = Normalize::initString($data, 'description');
		$this->owner_name = Normalize::initString($data, 'owner_name');

	}
    public function getCoverImage(): ?string
    {
        if ($this->cover_image) {

            return UseImage::getAsset(json_encode($this->cover_image), 960, 540);
        }

        return null;
    }

    public function getImage(): array
    {
        $image_return = [];
        if($this->image){
            foreach ($this->image as $image){
                $image_return[] = UseImage::getAsset(json_encode($image), 960, 540);
            }
        }
        return $image_return;
    }
}