<?php

namespace App\Http\Controllers\Post;

use App\Consts\DateFormat;
use App\Consts\Schema\DBPostFields;
use App\Http\Controllers\BaseController;
use App\Libs\NorIntoDB;
use App\Libs\QueryFields;
use App\Models\Post\PostModel;
use App\Models\User;
use App\Structs\Post\PostStruct;
use App\Structs\Struct;
use App\Structs\User\UserStruct;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\Uid\Ulid;

class PostController extends BaseController
{
    public function addPost(Request $request): JsonResponse
    {
        $rule = [
            'title'                  => 'required|between:3,100',
            'monthly_rent_cost'      => 'required|integer|min:1',
            'type'                   => 'required|in:Phòng trọ,Tìm ở ghép,Nhà/căn hộ cho thuê,Tìm phòng',
            'area'                   => 'required|numeric|min:1',
            'rental_status'          => 'required|in:Còn phòng,Nhượng phòng,Tìm bạn ở ghép,Đã đầy',
            'description'            => 'required',
            'address'                => 'required',
            'owner_name'             => 'required',
            'owner_phone_number'     => 'required|between:9,10',
            'image'                  => 'required',
            'image.*'                => 'image',
            'cover_image'            => 'required|image',
            'additional_information' => '',
        ];

        $message = [
            'title.required' => trans('post/post.error_title_required'),
            'title.between'  => trans('post/post.error_title_between', [
                'min' => 3,
                'max' => 100,
            ]),

            'monthly_rent_cost.required' => trans('post/post.error_monthly_rent_cost_required'),
            'monthly_rent_cost.integer'  => trans('post/post.error_monthly_rent_cost_type'),
            'monthly_rent_cost.min'      => trans('post/post.error_monthly_rent_cost_range', ['min' => 0]),

            'type.required' => trans('post/post.error_type_required'),
            'type.in'       => trans('post/post.error_type_range'),

            'area.required' => trans('post/post.error_area_required'),
            'area.numeric'  => trans('post/post.error_area_type'),
            'area.min'      => trans('post/post.error_area_range', ['min' => 0]),

            'rental_status.required' => trans('post/post.error_rental_status_required'),
            'rental_status.in'       => trans('post/post.error_rental_status_range'),

            'description.required' => trans('post/post.error_description_required'),

            'address.required' => trans('post/post.error_address_required'),

            'owner_name.required' => trans('post/post.error_owner_name_required'),

            'owner_phone_number.required' => trans('post/post.error_owner_phone_number_required'),
            'owner_phone_number.between'  => trans('post/post.error_owner_phone_number_between', [
                'min' => 9,
                'max' => 10,
            ]),

            'image.required' => trans('post/post.error_image_required'),
            'image.*'        => trans('post/post.error_image_type'),

            'cover_image.required' => trans('post/post.error_cover_image_required'),
            'cover_image.image'    => trans('post/post.error_cover_image_type'),
        ];
        $validator = $this->_validate($request, $rule, $message);

        if ($validator->errors()->count()) {
            $json = [
                'error' => firstError($validator->getMessageBag()->toArray())
            ];
        } else {
            if ($request->file('cover_image')) {
                $cover_image = doImage($request->file('cover_image'), 960, 540);
            }

            if ($list_img = $request->file('image')) {
                foreach ($list_img as $img) {
                    $image[] = doImage($img, 960, 540);
                }
            }
            $data = [
                'id'                     => $image['id'] ?? Ulid::generate(),
                'user_id'                => $request->user()->getAttribute('id'),
                'created_at'             => now()->format(DateFormat::TIMESTAMP_DB),
                'title'                  => $request->input('title'),
                'monthly_rent_cost'      => $request->input('monthly_rent_cost'),
                'type'                   => $request->input('type'),
                'area'                   => $request->input('area'),
                'rental_status'          => $request->input('rental_status'),
                'description'            => $request->input('description'),
                'owner_name'             => $request->input('owner_name'),
                'owner_phone_number'     => $request->input('owner_phone_number'),
                'cover_image'            => $cover_image ?? null,
                'image'                  => $image ?? null,
                'additional_information' => $request->input('additional_information') ?? null,
                'address'                => $request->input('address'),
            ];
            $diary_struct = new PostStruct($data);

            $data = normalizeToSQLViaArray($data, DBPostFields::POSTS);

            if ($data && PostModel::doAdd($data)) {
                $json = [
                    'data' => $diary_struct->toArray([
                        Struct::OPT_CHANGE => [
                            'cover_image' => ['getCoverImage'],  // process image by function inside struct
                            'image'       => ['getImage']
                        ],
                    ]),
                    'code' => 200,
                ];

            } else {
                $json = [
                    'code'  => 200, //400,
                    'error' => [
                        'warning' => trans('v1/default.error_insert'),
                    ]
                ];
            }
        }

        return resJson($json);
    }

    public function getPosts(Request $request): JsonResponse
    {
        /**@var $post_struct PostStruct
         * @var $user_access UserStruct
         * @var $post PostModel
         * */

        $rule = [
            'sort'      => 'in:asc,desc',
            'sort_by'   => 'in:created_at,name,id,user_id',
            'search_by' => '',
        ];

        $message = [
            'sort_by'   => trans('v1/default.error_selected', ['field' => 'sort_by', 'option' => 'created_at,name,id,user_id']),
            'sort'      => trans('v1/default.error_selected', ['field' => 'sort', 'option' => 'asc, desc']),
            'search_by' => trans('v1/default.error_selected', ['field' => 'search_by', 'option' => 'name']),
        ];

        $validator = $this->_validate($request, $rule, $message);

        if ($validator->errors()->count()) {
            $json = [
                'error' => firstError($validator->getMessageBag()->toArray())
            ];
        } else {
            $fields_diary = new QueryFields($request, DBPostFields::POSTS);
            $filter_data = [
                'fields'    => $fields_diary->select,
                ...pageLimit($request),
                'user_id'   => $request->user()->getAttribute('id'),
                'sort_by'   => $request->input('sort_by') ?? null,
                'sort'      => $request->input('sort') ?? 'asc',
                'search_by' => $request->input('search_by') ?? null,
                'filter_by' => $request->input('filter_by') ?? null,
                //                'key'       => "%{$request->input('key')}%" ?? '%%',
            ];
            if ($query = PostModel::doGet($filter_data)) {
                foreach ($query as $diary) {
                    $diary_struct = $diary->struct();
                    $data[] = $diary_struct->toArray([
                        Struct::OPT_CHANGE => [
                            'cover_image' => ['getCoverImage'],  // process image by function inside struct
                            'image'       => ['getImage']
                        ],
                    ]);
                }
            }
            $json = [
                'items' => $data ?? [''],
                '_meta' => ResMetaJson($query),
            ];

        }
        return resJson($json);
    }

    public function getPostsByUID(Request $request, string $id): JsonResponse
    {
        /**@var $post_struct PostStruct
         * @var $user_access UserStruct
         * @var $post PostModel
         * */

        $fields_diary = new QueryFields($request, DBPostFields::POSTS);
        $filter_data = [
            'fields'    => $fields_diary->select,
            ...pageLimit($request),
            'user_id'   => $request->user()->getAttribute('id'),
            'sort_by'   => $request->input('sort_by') ?? null,
            'sort'      => $request->input('sort') ?? 'asc',
            'search_by' => $request->input('search_by') ?? null,
            'filter_by' => $request->input('filter_by') ?? null,
            //                'key'       => "%{$request->input('key')}%" ?? '%%',
        ];
        $filter_data['search_by']['user_id'] = $id;
        if ($query = PostModel::doGet($filter_data)) {
            foreach ($query as $diary) {
                $diary_struct = $diary->struct();
                $data[] = $diary_struct->toArray([
                    Struct::OPT_CHANGE => [
                        'cover_image' => ['getCoverImage'],  // process image by function inside struct
                        'image'       => ['getImage']
                    ],
                ]);
            }
        }
        $json = [
            'items' => $data ?? [''],
            '_meta' => ResMetaJson($query),
        ];


        return resJson($json);
    }

    public function getPost(Request $request, string $id): JsonResponse
    {
        /**@var $post PostModel* */
        $fields_diary = new QueryFields($request, DBPostFields::POSTS);

        if ($post = PostModel::doGetById($id, $fields_diary->select)) {
            $json = [
                'data' => $post->struct()->toArray([
                    Struct::OPT_CHANGE => [
                        'cover_image' => ['getCoverImage'],  // process image by function inside struct
                        'image'       => ['getImage']
                    ],
                ])
            ];

        } else {
            $json = [
                'code'  => 200, //400,
                'error' => [
                    'id|user_id' => trans('v1/default.error_id_exists')
                ]
            ];
        }

        return resJson($json);
    }

    public function updatePost(Request $request, string $id): JsonResponse
    {
        /**@var $post_struct PostStruct
         * @var $post PostModel
         * */
        $accept_fields = [
            'rental_status'
        ];

        $post = PostModel::doGetById($id, ['*']);
        if ($post) {
            $post_data = normalizeToRequest($request->only($accept_fields), DBPostFields::POSTS);
            $nor_into = new NorIntoDB();
            if ($nor_into->viaDB($post_data, $post->getAttributes())) {

                $update = $nor_into instanceof NorIntoDB ? $nor_into->getData() : $nor_into;

                if (!isset($update['updated_at'])) {
                    $update['updated_at'] = now()->format(DateFormat::TIMESTAMP_DB);
                }

                //remove old diary's image in storage
                //DeleteImageJob->dispatch();

                PostModel::doEdit($update, $post);

                $post_struct = $post->struct();
                $json = [
                    'data' => $post_struct->toArray([
                        Struct::OPT_CHANGE => [
                            'image' => ['getImage']  // process image by function inside struct
                        ],
                        Struct::OPT_FILTER => array_keys($update)
                    ]),
                    'code' => 200,
                ];
            } else {
                $json = [
                    'error' => [
                        'warning' => 'Không có thay đổi',
                    ],
                    'code'  => 200,
                ];

            }
        } else {
            $json = [
                'error' => [
                    'warning' => 'Không tìm thấy để xoá',
                ],
                'code'  => '200'
            ];
        }
        return resJson($json);
    }

    public function deletePost(Request $request, string $id): JsonResponse
    {
        /**@var $post PostModel * */
        $post = PostModel::doGetById($id, ['*']);
        if ($post) {
            PostModel::doDelete($post);
            return resJson([
                'state' => [
                    'message' => 'Xoá thành công',
                ],
                'code'  => 200,
            ]);
        } else {
            return resJson([
                'error' => [
                    'warning' => 'Không tìm thấy để xoá',
                ],
                'code'  => 200,
            ]);
        }

    }

    protected function _validate(Request $request, ?array $rule = [], ?array $message = []): \Illuminate\Contracts\Validation\Validator|\Illuminate\Validation\Validator
    {
        $validator = Validator::make($request->all(), $rule, $message);
        if (!$validator->fails()) {
            $this->getUser($request);
            if (!$this->user instanceof User) {
                $validator->errors()->add('user', trans('v1/auth.error_username_not_exist'));
            }
        }

        return $validator;
    }
}
