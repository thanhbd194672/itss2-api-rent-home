<?php

return [
    'error_title_required' => 'Bạn chưa nhập tiêu đề bài đăng',
    'error_title_between'  => 'Tiêu đề phải gồm :min đến :max kí tự',

    'error_monthly_rent_cost_required' => "Bạn chưa nhập giá cho thuê phòng",
    'error_monthly_rent_cost_type'     => "Giá cho thuê phòng phải là số nguyên đơn vị VNĐ",
    'error_monthly_rent_cost_range'    => "Giá cho thuê phòng phải là lơn hơn :min",

    'error_type_required' => "Bạn chưa nhập Loại hình cho thuê",
    'error_type_range'    => "Loại hình cho thuê phải là một trong bốn giá trị đã cho",

    'error_area_required' => "Bạn chưa nhập diện tích phòng",
    'error_area_type'     => "Diện tích phòng phải là số thực",
    'error_area_range'    => "Diện tích phòng phải lớn hơn :min đơn vị m^2",

    'error_rental_status_required' => "Bạn chưa nhập trạng thái phòng",
    'error_rental_status_range'    => "Trạng thái phòng phải là một trong bốn giá trị đã cho",

    'error_description_required' => "Bạn chưa nhập thông tin mô tả phòng",

    'error_address_required' => "Bạn chưa nhập địa chỉ phòng trọ",

    'error_owner_name_required' => "Bạn chưa nhập tên chủ phòng trọ",

    'error_owner_phone_number_required' => "Bạn chưa nhập số điện thoại của chủ phòng trọ",
    'error_owner_phone_number_between'  => "Số điện thoại của chủ trọ phải là :min đến :max số",

    'error_image_required'       => "Bạn chưa thêm ảnh cho phòng trọ",
    'error_image_type'           => "Ảnh phòng trọ bạn đăng không đúng kiểu",
    //
    'error_cover_image_required' => "Bạn chưa thêm ảnh bìa cho phòng trọ",
    'error_cover_image_type'     => "Ảnh bìa phòng trọ bạn đăng không đúng kiểu",

];