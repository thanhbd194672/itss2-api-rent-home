<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/
Route::get('/', function () {
    return view('welcome');
});

Route::post('auth/login', 'Auth\LoginController@index');
Route::middleware('auth:jwt')->group(function () {
    Route::group(['namespace' => 'Post', 'prefix' => 'post'], function () {
        Route::get('gets', 'PostController@getPosts');
        Route::get('get/{id}', 'PostController@getPost');
        Route::get('getByUser/{id}', 'PostController@getPostsByUID');

        Route::post('add', 'PostController@addPost');
        Route::post('update/{id}', 'PostController@updatePost');
        Route::post('delete/{id}', 'PostController@deletePost');
    });
});
